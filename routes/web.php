<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.welcome');
})->name('home');

#Auth::routes();
Auth::routes(['register' => false]);

#Route::get('/home', 'HomeController@index')->name('home');
Route::redirect('/home', '/');

Route::get('/erreur/{msg}', 'HomeController@erreur')->name('erreur');

Route::get('/types_taxis', 'HomeController@types_taxis')->name('types_taxis');

Route::get('/infos_taxis', 'HomeController@infos_taxis')->name('infos_taxis');
Route::get('/modif_taxi/{id?}', 'HomeController@modif_taxi')->name('modif_taxi');
Route::post('/upsert_taxi', 'HomeController@upsert_taxi')->name('upsert_taxi');

Route::get('/infos_chauffeurs', 'HomeController@infos_chauffeurs')->name('infos_chauffeurs');
Route::get('/modif_chauffeur/{id?}', 'HomeController@modif_chauffeur')->name('modif_chauffeur');
Route::post('/upsert_chauffeur', 'HomeController@upsert_chauffeur')->name('upsert_chauffeur');