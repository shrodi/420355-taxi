@extends('layouts.welcome')

@section('styles')
    .infostaxis {
        font-size: large !important;
    }
@endsection

@section('contenu')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Description / Nb de places</th>
                <th scope="col">Immatriculation</th>
                <th scope="col">Assurance</th>
                <th scope="col">Kilométrage</th>
                <th scope="col">Date de mise en circulation</th>
                <th scope="col">Recette du taximètre</th>
                <th scope="col">Kilométrage taximètre</th>
                <th scope="col">Kilométrage chargé</th>
                <th scope="col">Nombre de courses</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($infosTaxis as $infoTaxi)
                <tr>
                    <th scope="row"><a href="{{ route('modif_taxi', ['id' => $infoTaxi->id_taxi]) }}">{{ $infoTaxi->id_taxi }}</a></th>
                    <td>{{ $infoTaxi->descriptionTaxi }} / {{ $infoTaxi->nombrePlaceTaxi }}</td>
                    <td>{{ $infoTaxi->immatriculation }}</td>
                    <td>{{ $infoTaxi->assurance }}</td>
                    <td>{{ $infoTaxi->millageTaxi }}</td>
                    <td>{{ $infoTaxi->dateMiseCirculation }}</td>
                    <td>{{ $infoTaxi->recetteTaximetre }}</td>
                    <td>{{ $infoTaxi->millageTaximetre }}</td>
                    <td>{{ $infoTaxi->millageChargeTaximetre }}</td>
                    <td>{{ $infoTaxi->priseChargeTaximetre }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th scope="col" colspan="999">
                    <a href="{{ route('modif_taxi') }}">Ajouter un taxi</a>
                </th> 
            </tr>
        </tfoot>
    </table>
@endsection