@extends('layouts.welcome')


@section('styles')
    .erreur {
        font-size: large !important;
        color: red !important;
        font-weight: bold !important;
    }
@endsection


@section('contenu')
    <p class="erreur">
        {{ $msg }}
    </p>
@endsection