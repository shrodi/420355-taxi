@extends('layouts.welcome')


@section('styles')
    .typestaxis {
        font-size: large !important;
    }
@endsection


@section('contenu')
    <table class="table">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Description</th>
              <th scope="col">Nombre de places</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($typesTaxis as $typeTaxi)
                <tr>
                    <th scope="row">{{ $typeTaxi->id_typeTaxi }}</th>
                    <td>{{ $typeTaxi->descriptionTaxi }}</td>
                    <td>{{ $typeTaxi->nombrePlaceTaxi }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection