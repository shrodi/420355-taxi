@extends('layouts.welcome')


@section('styles')
    .ajouttaxi {
        font-size: large !important;
    }
@endsection

@section('contenu')
    <form method="post" action="/upsert_taxi">
        @csrf
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Description / Nb de places</th>
                    <th scope="col">Immatriculation</th>
                    <th scope="col">Assurance</th>
                    <th scope="col">Kilométrage</th>
                    <th scope="col">Date de mise en circulation</th>
                    <th scope="col">Recette du taximètre</th>
                    <th scope="col">Kilométrage taximètre</th>
                    <th scope="col">Kilométrage chargé</th>
                    <th scope="col">Nombre de courses</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($infosTaxis as $infoTaxi)
                    <tr>
                        <th scope="row"><input type="hidden" name="id_taxi" value="{{ $infoTaxi->id_taxi }}">{{ $infoTaxi->id_taxi }}</th>
                        <td>
                            <select name="id_typeTaxi">
                                <option value=""></option>
                                @foreach ($typesTaxis as $typeTaxi)
                                    <option value="{{ $typeTaxi->id_typeTaxi }}"
                                        @if ($typeTaxi->id_typeTaxi == $infoTaxi->id_typeTaxi)
                                            selected
                                        @endif >
                                        {{ $typeTaxi->descriptionTaxi }} / {{ $typeTaxi->nombrePlaceTaxi }}
                                    </option>
                                @endforeach
                            </select>
                        </td>
                        <td><input type="text" name="immatriculation" size="8" value="{{ $infoTaxi->immatriculation }}"></td>
                        <td><input type="text" name="assurance" size="8" value="{{ $infoTaxi->assurance }}"></td>
                        <td><input type="number" name="millageTaxi" value="{{ $infoTaxi->millageTaxi }}"></td>
                        <td><input type="date" name="dateMiseCirculation" value="{{ $infoTaxi->dateMiseCirculation }}"></td>
                        <td><input type="number" name="recetteTaximetre" value="{{ $infoTaxi->recetteTaximetre }}"></td>
                        <td><input type="number" name="millageTaximetre" value="{{ $infoTaxi->millageTaximetre }}"></td>
                        <td><input type="number" name="millageChargeTaximetre" value="{{ $infoTaxi->millageChargeTaximetre }}"></td>
                        <td><input type="number" name="priseChargeTaximetre" value="{{ $infoTaxi->priseChargeTaximetre }}"></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <input type="submit" value="Soumettre">
    </form>
@endsection