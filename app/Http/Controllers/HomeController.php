<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    /*
     * Message d'erreur
     */
    public function erreur($msg) {
        return view('erreur', ['msg' => $msg]);
    }
    
    
    
    
    /*********************************************************/
    /********************  TAXIS *****************************/
    /*********************************************************/

    
    /**
     * Récupère et affiche les types de taxis
     */
    public function types_taxis()
    {
        $typesTaxis = DB::table('type_taxi')->get();
        return view('types_taxis', ['typesTaxis' => $typesTaxis]);
    }
    
        
    /**
     * Récupère et affiche les informations des taxis
     */
    public function infos_taxis()
    {
        $infosTaxis = DB::table('taxi')->join('type_taxi', 'type_taxi.id_typeTaxi', '=', 'taxi.id_typeTaxi')->orderBy('id_taxi')->get();
        return view('infos_taxis', ['infosTaxis' => $infosTaxis]);
    }
    
    
    /**
     * Récupère les informations d'un taxi existant pour les modifier, ou sinon affiche un formulaire vierge pour en ajouter un nouveau
     */
    public function modif_taxi($id_taxi = null)
    {
        // Tableau qui contiendra les infos de taxis
        $infosTaxis = [];
        
        // Récupère les infos de taxi si on a passé un id_taxi en argument, sinon crée un enregistrement vide
        if (!is_null($id_taxi))
        {
            $infosTaxis = DB::table('taxi')->join('type_taxi', 'type_taxi.id_typeTaxi', '=', 'taxi.id_typeTaxi')->where('id_taxi', $id_taxi)->get();
        }
        else
        {
            // Crée un enregistrement vide avec une classe anonyme
            $taxi = new class() {
                public $id_taxi = null;
                public $id_typeTaxi = null;
                public $immatriculation = null;
                public $assurance = null;
                public $millageTaxi = null;
                public $dateMiseCirculation = null;
                public $recetteTaximetre = null;
                public $millageTaximetre = null;
                public $millageChargeTaximetre = null;
                public $priseChargeTaximetre = null;
            };            
            $infosTaxis = [
                $taxi
            ];
        }
        
        $typesTaxis = DB::table('type_taxi')->get();
        return view('modif_taxi', ['infosTaxis' => $infosTaxis, 'typesTaxis' => $typesTaxis]);
    }
    
    
    /**
     * Insère un nouveau taxi ou met à jour les infos d'un taxi existant à partir de la requête POST
     * reçue du formulaire de saisie
     */
    public function upsert_taxi(Request $request)
    {
        // Trouve le id_taxi max dans la BD
        $id_taxi_max = DB::table('taxi')->max('id_taxi');
            
        /* On doit valider les entrées avant MAJ de la table afin d'éviter les
         * attaques par injection SQL, sinon on redirige vers une page d'erreur
         */
        if (!$this->valide_taxi($request, $id_taxi_max, $err)) {
            return redirect()->route('erreur', ['msg' => $err]);
        }
        
        
        // Récupère l'id de taxi à modifier
        $id_taxi = $request->input('id_taxi');
        
        /*
         * Si aucun id_taxi n'a été transmis, c'est qu'il s'agit d'un nouveau taxi et donc il faut 
         * générer un nouveau id_taxi puisque la colonne n'est pas AUTO_INCREMENT
         */
        if (is_null($id_taxi))
        {
            // Le nouveau id_taxi = max(id_taxi) existant dans la table + 1
            $id_taxi = $id_taxi_max + 1;
        }
        
        
        DB::table('taxi')->updateOrInsert(
            ['id_taxi' => $id_taxi],
            [
                'id_typeTaxi' => $request->input('id_typeTaxi'),
                'immatriculation' => $request->input('immatriculation'),
                'assurance' => $request->input('assurance'),
                'millageTaxi' => $request->input('millageTaxi'),
                'dateMiseCirculation' => $request->input('dateMiseCirculation'),
                'recetteTaximetre' => $request->input('recetteTaximetre'),
                'millageTaximetre' => $request->input('millageTaximetre'),
                'millageChargeTaximetre' => $request->input('millageChargeTaximetre'),
                'priseChargeTaximetre' => $request->input('priseChargeTaximetre')
            ]
        );
        
        return redirect()->route('infos_taxis');
    }
    
    /*
     * Vérifie si les données entrées pour un taxi sont valides
     */
    private function valide_taxi(Request $request, $id_taxi_max, &$err) {
        // Valide à moins de rencontrer un problème
        $valide = true;
        
        if (
            (!( // L'id_taxi peut être null ou sinon doit être un entier <= $id_taxi_max
                is_null($request->input('id_axi')) ||
                (
                    preg_match('/\d+/', $request->input('id_axi')) &&
                    $request->input('id_axi') <= $id_taxi_max
                )
              ) && $err = "id_taxi invalide (${$request->input('id_axi')})"
            ) ||
            (
                !preg_match('/^\d+$/', $request->input('id_typeTaxi')) && $err = "Type de taxi invalide (".$request->input('id_typeTaxi').")"
            ) ||
            (
                !preg_match('/^\w{6}$/', $request->input('immatriculation')) && $err = "Immatriculation invalide (".$request->input('immatriculation').")"
            ) ||
            (
                !preg_match('/^\w{3,20}$/', $request->input('assurance')) && $err = "Assurance invalide (".$request->input('assurance').")"
            ) ||
            (
                !preg_match('/^\d+$/', $request->input('millageTaxi')) && $err = "Kilométrage taxi invalide (".$request->input('millageTaxi').")"
            ) ||
            (
                !preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $request->input('dateMiseCirculation')) && $err = "Date de mise en circulation invalide (".$request->input('dateMiseCirculation').")"
            ) ||
            (
                !preg_match('/^\d*[,\.]?\d*$/', $request->input('recetteTaximetre')) && $err = "Recette taximètre invalide (".$request->input('recetteTaximetre').")"
            ) ||
            (
                !preg_match('/^\d+$/', $request->input('millageTaximetre')) && $err = "Kilométrage taximètre invalide (".$request->input('millageTaximetre').")"
            ) ||
            (
                !preg_match('/^\d+$/', $request->input('millageChargeTaximetre')) && $err = "Kilométrage chargé invalide (".$request->input('millageChargeTaximetre').")"
            ) ||
            (
                !preg_match('/^\d+$/', $request->input('priseChargeTaximetre')) && $err = "Nombre de courses invalide (".$request->input('priseChargeTaximetre').")"
            )
            
        ) {$valide = false;}
        
        return $valide;
    }
    
    
    
    /*********************************************************/
    /********************  CHAUFFEURS ************************/
    /*********************************************************/
    
    /**
     * Récupère et affiche les informations des chauffeurs
     */
    public function infos_chauffeurs()
    {
        $infosChauffeurs = DB::table('chauffeur')->orderBy('id_chauffeur')->get();
        return view('infos_chauffeurs', ['infosChauffeurs' => $infosChauffeurs]);
    }
    
    
    /**
     * Récupère les informations d'un chauffeur existant pour les modifier, ou sinon affiche un formulaire vierge pour en ajouter un nouveau
     */
    public function modif_chauffeur($id_chauffeur = null)
    {
        // Tableau qui contiendra les infos de chauffeurs
        $infosChauffeurs = [];
        
        // Récupère les infos de chauffeur si on a passé un id_chauffeur en argument, sinon crée un enregistrement vide
        if (!is_null($id_chauffeur))
        {
            $infosChauffeurs = DB::table('chauffeur')->where('id_chauffeur', $id_chauffeur)->get();
        }
        else
        {
            // Crée un enregistrement vide avec une classe anonyme
            $chauffeur = new class() {
                public $id_chauffeur = null;
                public $nomChauffeurhauffeur = null;
                public $prenomChauffeurhauffeur = null;
                public $cellChauffeur = null;
                public $courrielChauffeur = null;
                public $adresseChauffeur = null;
                public $noPermisChauffeur = null;
                public $dateExpPermisChauffeur = null;
                public $commissionChauffeur = null;
            };            
            $infosChauffeurs = [
                $chauffeur
            ];
        }
        
        return view('modif_chauffeur', ['infosChauffeurs' => $infosChauffeurs]);
    }
    
    
    /**
     * Insère un nouveau chauffeur ou met à jour les infos d'un chauffeur existant à partir de la requête POST
     * reçue du formulaire de saisie
     */
    public function upsert_chauffeur(Request $request)
    {
        // Trouve le id_chauffeur max dans la BD
        $id_chauffeur_max = DB::table('chauffeur')->max('id_chauffeur');
            
        /* On doit valider les entrées avant MAJ de la table afin d'éviter les
         * attaques par injection SQL, sinon on redirige vers une page d'erreur
         */
        if (!$this->valide_chauffeur($request, $id_chauffeur_max, $err)) {
            return redirect()->route('erreur', ['msg' => $err]);
        }
        
        
        // Récupère l'id de chauffeur à modifier
        $id_chauffeur = $request->input('id_chauffeur');
        
        /*
         * Si aucun id_chauffeur n'a été transmis, c'est qu'il s'agit d'un nouveau chauffeur et donc il faut 
         * générer un nouveau id_chauffeur puisque la colonne n'est pas AUTO_INCREMENT
         */
        if (is_null($id_chauffeur))
        {
            // Le nouveau id_chauffeur = max(id_chauffeur) existant dans la table + 1
            $id_chauffeur = $id_chauffeur_max + 1;
        }
        
        
        DB::table('chauffeur')->updateOrInsert(
            ['id_chauffeur' => $id_chauffeur],
            [
                'nomChauffeurhauffeur' => $request->input('nom'),
                'prenomChauffeurhauffeur' => $request->input('prenom'),
                'cellChauffeur' => $request->input('cell'),
                'courrielChauffeur' => $request->input('courriel'),
                'adresseChauffeur' => $request->input('adresse'),
                'noPermisChauffeur' => $request->input('permis'),
                'dateExpPermisChauffeur' => $request->input('date_exp'),
                'commissionChauffeur' => $request->input('commission')
            ]
        );
        
        return redirect()->route('infos_chauffeurs');
    }
    
    /*
     * Vérifie si les données entrées pour un chauffeur sont valides
     */
    private function valide_chauffeur(Request $request, $id_chauffeur_max, &$err) {
        // Valide à moins de rencontrer un problème
        $valide = true;
        
        if (
            (!( // L'id_chauffeur peut être null ou sinon doit être un entier <= $id_chauffeur_max
                is_null($request->input('id_axi')) ||
                (
                    preg_match('/\d+/', $request->input('id_axi')) &&
                    $request->input('id_axi') <= $id_chauffeur_max
                )
              ) && $err = "id_chauffeur invalide (${$request->input('id_axi')})"
            ) ||
            (
                !preg_match("/^[a-z\s']{2,30}$/i", $request->input('nom')) && $err = "Nom invalide (".$request->input('nom').")"
            ) ||
            (
                !preg_match("/^[a-z ']{2,30}$/i", $request->input('prenom')) && $err = "Prénom invalide (".$request->input('prenom').")"
            ) ||
            (
                !preg_match('/^[\d\- ]{7,15}$/', $request->input('cell')) && $err = "Cellulaire invalide (".$request->input('cell').")"
            ) ||
            (
                !preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $request->input('date_exp')) && $err = "Date invalide (".$request->input('date_exp').")"
            ) ||
            (
                !preg_match('/^[\w\.\-]*[\w\-]+@[\w\.\-]+\.\w{2,}$/', $request->input('courriel')) && $err = "Courriel invalide (".$request->input('courriel').")"
            ) ||
            (
                !preg_match('/^.{3,75}$/', $request->input('adresse')) && $err = "Adresse invalide (".$request->input('adresse').")"
            ) ||
            (
                !preg_match('/^.{3,20}$/', $request->input('permis')) && $err = "Permis invalide (".$request->input('permis').")"
            ) ||
            (
                !preg_match('/^\d(?:\.\d{1,2})?$/', $request->input('commission')) && $err = "Commission invalide (".$request->input('commission').")"
            )
            
        ) {$valide = false;}
        
        return $valide;
    }
}
