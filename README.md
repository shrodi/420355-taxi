### Cours 420-355-LI, A2019, TP3

## Exemple de solution

Pour faire fonctionner:

1. Git clone ou copie du contenu du présent dossier dans le dossier **laragon/www/taxi**
1. Dans le terminal Laragon, aller dans le dossier **taxi** et entrer la commande `composer install`
1. Entrer ensuite la commande `mysql -u root <taxi.sql` (MySQL doit être actif avec un usager root sans mot de passe, sinon modifier la commande en conséquence)

### Login

- Nom d'usager: prof@cegep.com
- Mot de passe: abcd1234