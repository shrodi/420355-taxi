@extends('layouts.welcome')


@section('styles')
    .ajoutchauf {
        font-size: large !important;
    }
@endsection

@section('contenu')
    <form method="post" action="/upsert_chauffeur">
        @csrf
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Cellulaire</th>
                    <th scope="col">Courriel</th>
                    <th scope="col">Adresse</th>
                    <th scope="col">Permis</th>
                    <th scope="col">Expiration permis</th>
                    <th scope="col">Commission</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($infosChauffeurs as $infoChauffeur)
                    <tr>
                        <th scope="row"><input type="hidden" name="id_chauffeur" value="{{ $infoChauffeur->id_chauffeur }}">{{ $infoChauffeur->id_chauffeur }}</th>
                        <td><input type="text" name="nom" size="8" value="{{ $infoChauffeur->nomChauffeurhauffeur }}"></td>
                        <td><input type="text" name="prenom" size="8" value="{{ $infoChauffeur->prenomChauffeurhauffeur }}"></td>
                        <td><input type="text" name="cell" size="8" value="{{ $infoChauffeur->cellChauffeur }}"></td>
                        <td><input type="email" name="courriel" size="8" value="{{ $infoChauffeur->courrielChauffeur }}"></td>
                        <td><input type="text" name="adresse" size="8" value="{{ $infoChauffeur->adresseChauffeur }}"></td>
                        <td><input type="text" name="permis" size="8" value="{{ $infoChauffeur->noPermisChauffeur }}"></td>
                        <td><input type="date" name="date_exp" value="{{ $infoChauffeur->dateExpPermisChauffeur }}"></td>
                        <td><input type="number" step="0.01" name="commission" size="8" value="{{ $infoChauffeur->commissionChauffeur }}"></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <input type="submit" value="Soumettre">
    </form>
@endsection