-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: taxi
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `taxi`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `taxi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `taxi`;

--
-- Table structure for table `chauffeur`
--

DROP TABLE IF EXISTS `chauffeur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chauffeur` (
  `id_chauffeur` int(6) NOT NULL,
  `nomChauffeurhauffeur` varchar(30) NOT NULL,
  `prenomChauffeurhauffeur` varchar(30) NOT NULL,
  `cellChauffeur` varchar(15) NOT NULL,
  `courrielChauffeur` varchar(50) NOT NULL,
  `adresseChauffeur` varchar(75) NOT NULL,
  `noPermisChauffeur` varchar(20) NOT NULL,
  `dateExpPermisChauffeur` date NOT NULL,
  `commissionChauffeur` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id_chauffeur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chauffeur`
--

LOCK TABLES `chauffeur` WRITE;
/*!40000 ALTER TABLE `chauffeur` DISABLE KEYS */;
INSERT INTO `chauffeur` VALUES (1,'asdasd','asdasd','234-8765','e@mar.tel','sdfsd','sdfsdf','2019-12-13',2.34);
/*!40000 ALTER TABLE `chauffeur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('e@mar.tel','$2y$10$tELgJgoaO3hCsD8oyhGJr.ZMwjia5N39bKWk7/UdFoRGubB4RVqoi','2019-12-11 01:50:10');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shift`
--

DROP TABLE IF EXISTS `shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift` (
  `id_shift` int(6) NOT NULL AUTO_INCREMENT,
  `id_taxi` int(5) NOT NULL,
  `id_chauffeur` int(6) NOT NULL,
  `dateDebutShift` datetime NOT NULL,
  `dateFinShift` datetime NOT NULL,
  `recetteArrShift` decimal(8,2) NOT NULL,
  `recetteDepShift` decimal(8,2) DEFAULT NULL,
  `prixFixeShift` decimal(6,2) DEFAULT NULL,
  `millageArrShift` int(6) NOT NULL,
  `millageDepShift` int(6) DEFAULT NULL,
  `millageChargeArrShift` int(6) NOT NULL,
  `millageChargeDepShift` int(6) DEFAULT NULL,
  `priseChargeArrShift` int(6) NOT NULL,
  `priseChargeDepShift` int(6) DEFAULT NULL,
  `millageAutoArrShift` int(6) NOT NULL,
  `millageAutoDepShift` int(6) DEFAULT NULL,
  `gazShift` decimal(5,2) DEFAULT NULL,
  `creditShift` decimal(6,2) DEFAULT NULL,
  `diversShift` decimal(6,2) DEFAULT NULL,
  PRIMARY KEY (`id_shift`),
  KEY `fk_shift_Chauffeur` (`id_chauffeur`),
  KEY `fk_shift_taxi` (`id_taxi`),
  CONSTRAINT `fk_shift_Chauffeur` FOREIGN KEY (`id_chauffeur`) REFERENCES `chauffeur` (`id_chauffeur`),
  CONSTRAINT `fk_shift_taxi` FOREIGN KEY (`id_taxi`) REFERENCES `taxi` (`id_taxi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shift`
--

LOCK TABLES `shift` WRITE;
/*!40000 ALTER TABLE `shift` DISABLE KEYS */;
/*!40000 ALTER TABLE `shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxi`
--

DROP TABLE IF EXISTS `taxi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxi` (
  `id_taxi` int(5) NOT NULL,
  `immatriculation` varchar(6) NOT NULL,
  `assurance` varchar(20) NOT NULL,
  `millageTaxi` int(6) NOT NULL,
  `dateMiseCirculation` date NOT NULL,
  `recetteTaximetre` decimal(8,2) NOT NULL,
  `millageTaximetre` int(6) NOT NULL,
  `millageChargeTaximetre` int(6) NOT NULL,
  `priseChargeTaximetre` int(6) NOT NULL,
  `id_typeTaxi` int(2) NOT NULL,
  PRIMARY KEY (`id_taxi`),
  KEY `fk_taxi` (`id_typeTaxi`),
  CONSTRAINT `fk_taxi` FOREIGN KEY (`id_typeTaxi`) REFERENCES `type_taxi` (`id_typeTaxi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxi`
--

LOCK TABLES `taxi` WRITE;
/*!40000 ALTER TABLE `taxi` DISABLE KEYS */;
INSERT INTO `taxi` VALUES (1,'abc457','ass01',12000,'2017-12-10',567.89,1200,120,67,2),(2,'gdh67','876gh',20000,'2019-12-01',234.00,678,87,34,1),(3,'ghf678','hsgs8s5',98766,'2019-11-24',457.00,98765,345,78,2),(4,'34tgh7','12354',12,'2019-12-14',121.00,121,121,122,1);
/*!40000 ALTER TABLE `taxi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_taxi`
--

DROP TABLE IF EXISTS `type_taxi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_taxi` (
  `id_typeTaxi` int(2) NOT NULL,
  `descriptionTaxi` varchar(20) NOT NULL,
  `nombrePlaceTaxi` int(11) NOT NULL,
  PRIMARY KEY (`id_typeTaxi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_taxi`
--

LOCK TABLES `type_taxi` WRITE;
/*!40000 ALTER TABLE `type_taxi` DISABLE KEYS */;
INSERT INTO `type_taxi` VALUES (1,'Van',6),(2,'berline',4);
/*!40000 ALTER TABLE `type_taxi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Prof','prof@cegep.com',NULL,'$2y$10$1xMW6EdmfQnHMZWx9m5eEudOdBNP0UPTYNYo8dkO2SHOvTJ3iafGC',NULL,'2019-12-11 01:54:18','2019-12-11 01:54:18');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-14 11:16:21
