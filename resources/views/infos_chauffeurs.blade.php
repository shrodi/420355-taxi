@extends('layouts.welcome')

@section('styles')
    .infoschauf {
        font-size: large !important;
    }
@endsection

@section('contenu')
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Cellulaire</th>
                <th scope="col">Courriel</th>
                <th scope="col">Adresse</th>
                <th scope="col">Permis</th>
                <th scope="col">Expiration permis</th>
                <th scope="col">Commission</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($infosChauffeurs as $infoChauffeur)
                <tr>
                    <th scope="row"><a href="{{ route('modif_chauffeur', ['id' => $infoChauffeur->id_chauffeur]) }}">{{ $infoChauffeur->id_chauffeur }}</a></th>
                    <td>{{ $infoChauffeur->nomChauffeurhauffeur }}</td>
                    <td>{{ $infoChauffeur->prenomChauffeurhauffeur }}</td>
                    <td>{{ $infoChauffeur->cellChauffeur }}</td>
                    <td>{{ $infoChauffeur->courrielChauffeur }}</td>
                    <td>{{ $infoChauffeur->adresseChauffeur }}</td>
                    <td>{{ $infoChauffeur->noPermisChauffeur }}</td>
                    <td>{{ $infoChauffeur->dateExpPermisChauffeur }}</td>
                    <td>{{ $infoChauffeur->commissionChauffeur }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th scope="col" colspan="999">
                    <a href="{{ route('modif_chauffeur') }}">Ajouter un chauffeur</a>
                </th> 
            </tr>
        </tfoot>
    </table>
@endsection